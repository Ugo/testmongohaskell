{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Database.MongoDB
import Control.Monad.Trans (liftIO)
import Control.Monad.IO.Class
import Data.List.Split
import Control.Concurrent.Async (mapConcurrently)
import Control.Monad.Parallel as Par
import Data.Traversable as T

-- https://wiki.haskell.org/Data_declaration_with_constraint
{-
data MongoMyContext m a = MongoMyContext {
    run :: MonadIO m => Action m a -> m a
}
-}

type Chunk a = [a]
data BulkResult = BulkResult { chunks :: Int, docs :: Int } deriving Show

main :: IO ()
main = do
   pipe <- connect (host "127.0.0.1")
   let runAction = access pipe master "perfimmo"
   runAction myRun
   close pipe

myRun :: Action IO ()
myRun = do
    tickets <- prepareData
    result <- bulkInsertTicket tickets
    println $ show result

prepareData :: Action IO [Document]
prepareData = do
    clearTickets2
    allTickets

clearTickets2 :: Action IO ()
clearTickets2 = delete (select [] "detailedTickets2")

allTickets :: Action IO [Document]
allTickets = rest =<< find (select [] "detailedTickets")

bulkInsertTicket :: [Document] -> Action IO BulkResult
bulkInsertTicket tickets = do
    --r <- Par.sequence act
    r <- T.sequence act
    pure BulkResult { chunks = length r, docs = totalDocs r }
    where act :: [Action IO [Value]] = map (insertAll "detailedTickets2") (splitEvery 2000 tickets)
          totalDocs = length . concat

println :: String -> Action IO ()
println s = liftIO $ putStrLn s

-- TODO need sequence instead of mapConcurently
--  cf. https://hackage.haskell.org/package/monad-parallel-0.7.2.3/docs/Control-Monad-Parallel.html#v:sequence
bulkInsert :: {-MongoMyContext-} (Action IO [Value] -> IO [Value]) -> [Document] -> IO BulkResult
bulkInsert run docs = do
    r <- parChunks
    pure BulkResult { chunks = length r, docs = totalDocs r }
    where bulkOps :: [Action IO [Value]] = map (insertAll "detailedTickets2") (splitEvery 2000 docs)
          parChunks :: IO[[Value]] = Par.sequence $ fmap run bulkOps
          totalDocs = length . concat